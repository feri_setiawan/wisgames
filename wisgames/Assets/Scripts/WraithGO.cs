using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WraithGO : MonoBehaviour
{
    private Animator animator;

    public static WraithGO Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.finishGame)
        {
            animator.SetTrigger("Idle");
        }
    }

    public void Attack()
    {
        if (!(animator.GetCurrentAnimatorStateInfo(0).IsName("Attacking")))
            animator.SetTrigger("Attack");
    }

    public void Walk()
    {
        animator.SetTrigger("Walk");
    }

    void DealDamage()
    {
        animator.SetTrigger("Idle");
        GameManager.Instance.minotaurCurrentHealth -= GameManager.Instance.wraithInfo.attPower;
    }
}
