using UnityEngine;

[CreateAssetMenu(fileName = "HeroScriptableObject", menuName = "ScriptableObject/Hero")]
public class HeroScriptableObject : ScriptableObject
{
    public string heroName;
    public float health;
    public float attPower;
    public float attRate;
    public float attRange;
    public float speed;
}
