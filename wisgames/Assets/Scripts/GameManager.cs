using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{
    [Header("Hero Scriptable Object")]
    public HeroScriptableObject minotaurInfo;
    public HeroScriptableObject wraithInfo;

    [Header("UI")]
    [SerializeField]
    GameObject minotaurHealthGO;
    [SerializeField]
    Slider minotaurHealthSlider;
    [SerializeField]
    GameObject wraithHealthGO;
    [SerializeField]
    Slider wraithHealthSlider;
    [SerializeField]
    GameObject countDown;
    [SerializeField]
    GameObject finishGameGO;

    [HideInInspector]
    public float minotaurCurrentHealth;
    [HideInInspector]
    public float wraithCurrentHealth;

    bool minoIsAttacking = false;
    bool wraithIsAttacking = false;

    private float countDownRemaining = 3;
    bool startGame = false;

    [HideInInspector]
    public bool finishGame = false;

    public static GameManager Instance { get; set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {

    }


    void Update()
    {
        if (countDown.activeSelf)
        {
            if(countDownRemaining > 0)
            {
                countDownRemaining -= Time.deltaTime;
                countDown.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = Math.Round(countDownRemaining, 0).ToString();
            }
            else
            {
                countDown.SetActive(false);
                StartGame();
            }
        }
        if (startGame)
        {
            UpdateHealth();

            if (minotaurCurrentHealth < 0 || wraithCurrentHealth < 0)
            {
                finishGame = true;
                CancelInvoke();
                FinishGame();
            }
        }
    }

    void StartGame()
    {
        startGame = true;

        MinotaurGO.Instance.GetComponent<SpriteRenderer>().enabled = true;
        WraithGO.Instance.GetComponent<SpriteRenderer>().enabled = true;

        StartingHealth();
        StartCoroutine(HeroMove());

    }

    void FinishGame()
    {
        string winner;
        if (minotaurCurrentHealth > 0)
        {
            winner = minotaurInfo.heroName;
        }
        else
        {
            winner = wraithInfo.heroName;
        }

        finishGameGO.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Winner " + winner;
        finishGameGO.SetActive(true);
    }

    void StartingHealth()
    {
        minotaurCurrentHealth = minotaurInfo.health;
        wraithCurrentHealth = wraithInfo.health;

        minotaurHealthGO.SetActive(true);
        wraithHealthGO.SetActive(true);
    }

    void UpdateHealth()
    {
        minotaurHealthSlider.value = (float)(minotaurCurrentHealth / minotaurInfo.health);
        wraithHealthSlider.value = (float)(wraithCurrentHealth / wraithInfo.health);
    }

    IEnumerator HeroMove()
    {
        yield return new WaitForSeconds(2f);

        MinotaurGO.Instance.Walk();
        WraithGO.Instance.Walk();

        bool minoAtRange = !(Vector2.Distance(new Vector2(MinotaurGO.Instance.transform.position.x, 0), new Vector2(WraithGO.Instance.transform.position.x, 0f)) > minotaurInfo.attRange);
        bool wraithAtRange = !(Vector2.Distance(new Vector2(MinotaurGO.Instance.transform.position.x, 0), new Vector2(WraithGO.Instance.transform.position.x, 0f)) > wraithInfo.attRange);


        while (!minoAtRange || !wraithAtRange)
        {
            if (!minoAtRange)
            {
                MinotaurGO.Instance.transform.position += transform.right * minotaurInfo.speed * Time.deltaTime;
                minoAtRange = !(Vector2.Distance(new Vector2(MinotaurGO.Instance.transform.position.x, 0), new Vector2(WraithGO.Instance.transform.position.x, 0f)) > minotaurInfo.attRange);
            }
            else
            {
                InvokeRepeating("minoAtt", 0f, minotaurInfo.attRate + 1f);
                minoIsAttacking = true;
            }

            if (!wraithAtRange)
            {
                WraithGO.Instance.transform.position -= transform.right * wraithInfo.speed * Time.deltaTime;
                wraithAtRange = !(Vector2.Distance(new Vector2(MinotaurGO.Instance.transform.position.x, 0), new Vector2(WraithGO.Instance.transform.position.x, 0f)) > wraithInfo.attRange);
            }
            else
            {
                InvokeRepeating("wraithAtt", 0f, wraithInfo.attRate + 1f);
                wraithIsAttacking = true;
            }

            yield return null;
        }

        if (!minoIsAttacking)
        {
            InvokeRepeating("minoAtt", 0f, minotaurInfo.attRate+1f);
        }

        if(!wraithIsAttacking)
            InvokeRepeating("wraithAtt", 0f, wraithInfo.attRate+1f);



        //MinotaurGO.Instance.Attack();
        //WraithGO.Instance.Attack();
    }

    void minoAtt()
    {
        MinotaurGO.Instance.Attack();
    }

    void wraithAtt()
    {
        WraithGO.Instance.Attack();
    }

    public void LoadLobby()
    {
        SceneManagement.Instance.LoadLobbyScene();
    }
}
